#!/usr/bin/env python3

import os
import threading
import time

import cherrypy
import psutil


PATH = os.path.abspath(os.path.dirname(__file__))


class StatsThread(threading.Thread):
    def __init__(self):
        super().__init__()
        self.terminate = False
        self.cpu = 0
        self.sent = 0
        self.recv = 0
        self.temp = 0

    def stop(self):
        self.terminate = False

    def run(self):
        prev_sent = 0
        prev_recv = 0
        while not self.terminate:
            self.cpu = psutil.cpu_percent()
            net = psutil.net_io_counters()
            if prev_sent != 0:
                self.sent = net.bytes_sent-prev_sent
            prev_sent = net.bytes_sent
            if prev_recv != 0:
                self.recv = net.bytes_recv-prev_recv
            prev_recv = net.bytes_recv
            try:
            	with open('/sys/class/thermal/thermal_zone0/temp') as f:
                    self.temp = float(f.read())/1000
            except:
                pass
            time.sleep(1)


class Root(object):
    @cherrypy.expose
    @cherrypy.tools.json_out()
    def stats(self):
        return {'object': {'cpu': stats.cpu, 'sent': stats.sent, 'recv': stats.recv, 'temp': stats.temp}}


cherrypy.tree.mount(Root(), '/', config={
    '/': {
        'tools.staticdir.on': True,
        'tools.staticdir.dir': PATH,
        'tools.staticdir.index': 'index.html',
    },
})

stats = StatsThread()
stats.start()
cherrypy.server.socket_host = '0.0.0.0'
cherrypy.engine.start()
try:
    while True:
        time.sleep(1)
except KeyboardInterrupt:
    pass
stats.stop()
